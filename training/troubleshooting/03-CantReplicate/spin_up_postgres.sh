export PGDATA='/var/lib/postgresql/15/main'

# Build the Postgres queen image
docker build -t cantreplicatequeen queen &> build.log

# Run the queen
docker run -d --name 03-cantreplicatequeen cantreplicatequeen

# Get the queen's IP
export QUEEN=`docker exec -it 03-cantreplicatequeen /usr/sbin/ifconfig eth0 | awk '/inet/ { $2; print $2}'`

# Build the Postgres princess image
docker build -t cantreplicateprincess princess &> build.log

# Run the princess
docker run -it -d -p 5442:5432 --name 03-cantreplicateprincess cantreplicateprincess

# Get a backup from the queen
docker exec -it 03-cantreplicateprincess \
  /usr/bin/pg_basebackup -w -U replicator -X stream -R -c fast \
  -h ${QUEEN} -D ${PGDATA}

# Switch WALs on the queen
docker exec -it 03-cantreplicatequeen /usr/bin/psql \
  -c "select pg_switch_wal ();" -o /dev/null
docker exec -it 03-cantreplicatequeen /usr/bin/psql \
  -c "select pg_switch_wal ();" -o /dev/null
docker exec -it 03-cantreplicatequeen /usr/bin/psql \
  -c "select pg_switch_wal ();" -o /dev/null
docker exec -it 03-cantreplicatequeen /usr/bin/psql \
  -c "select pg_switch_wal ();" -o /dev/null

# Alter permissions on pgpass file
docker exec -it 03-cantreplicateprincess /usr/bin/chmod 666 var/lib/postgresql/.pgpass

