# syntax=docker/dockerfile:1
FROM ubuntu:20.04

# Explain to debian it's talking with a machine without any human interraction
ENV DEBIAN_FRONTEND noninteractive

# Install wget, gnupg and vim
RUN apt-get update && apt-get install -y wget gnupg vim iputils-ping sudo

# Add PostgreSQL's repository. It contains the most recent stable release
#  of PostgreSQL.
RUN echo "deb http://apt.postgresql.org/pub/repos/apt focal-pgdg main" > /etc/apt/sources.list.d/pgdg.list

# Add the PostgreSQL PGP key to verify their Debian packages.
# It should be the same key as https://www.postgresql.org/media/keys/ACCC4CF8.asc
RUN wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -

# Install PostgreSQL
#  There are some warnings (in red) that show up during the build. You can hide
#  them by prefixing each apt-get statement with DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install -y postgresql-15

# Expose the PostgreSQL port
EXPOSE 5432

# Note: The official Debian and Ubuntu images automatically ``apt-get clean``
# after each apt-get

# Run the rest of the commands as the postgres user
USER postgres

# Remove the PGDATA directory
RUN rm -rf /var/lib/postgresql/15/main/*

# Create the .pgpass file
RUN echo "*:5432:*:replicator:r3pl1c4t0r" > ~/.pgpass
RUN chmod 0600 ~/.pgpass

# Add VOLUMEs to allow backup of config, logs and databases
VOLUME  ["/etc/postgresql", "/var/log/postgresql", "/var/lib/postgresql"]

# Set the default command to run when starting the container
CMD ["/bin/sh", "-c", "bash"]
