Here's how to play with these containers. (Docker needs to be installed and an
Internet connection is highly recommended to pull the images.)


# Spin up the Postgres container

If you have a bash shell:
```
./spin_up_postgres.sh
```

Else:

- Use the dockerfile given to build the image
- Start the container using the built image

## Building the image

```
docker build -t imgname .
```

## Create the container

```
docker run -d -P --name name imgname
```

# List the containers

```
docker ps
```

# Start/Stop the container

```
docker stop containerid
docker start containerid
```

# Remove the container

```
docker rm [-f] containerid
```

# Connecting to the container

```
docker exec -it name /bin/whatever
```
