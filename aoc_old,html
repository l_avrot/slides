<!DOCTYPE html>
<html lang="en">
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Apple macOS version 5.8.0">
  <meta charset="utf-8">
  <title>Learning advanced SQL the weird (and hard) way</title>
  <meta name="description" content=
  "A framework for easily creating beautiful presentations using HTML">
  <link rel="stylesheet" href="reveal.js/css/reveal.css">
  <link rel="stylesheet" href="reveal.js/css/theme/black.css" id=
  "theme"><!-- Theme used for syntax highlighting of code -->
  <link rel="stylesheet" href="reveal.js/lib/css/monokai.css">
  <link rel="stylesheet" href="css/titia_pink.css">
  <!-- Printing and PDF exports -->

  <script>
      var link = document.createElement( 'link' );
      link.rel = 'stylesheet';
      link.type = 'text/css';
      link.href = window.location.search.match( /print-pdf/gi ) ?
  'reveal.js/css/print/pdf.css' : 'reveal.js/css/print/paper.css';
      document.getElementsByTagName( 'head' )[0].appendChild( link );
  </script><!--[if lt IE 9]>
    <script src="reveal.js/lib/js/html5shiv.js"></script>
    <![endif]-->
</head>
<body>
  <div class="reveal">
    <!-- Any section element inside of this container is displayed as a slide -->
    <div class="slides">
      <section>
        <section data-background-image="images/aoc/footprints.jpg"
        data-background-opacity="0.2" data-background-position=
        "center center">
          <h1 class="normalcase">Learning advanced SQL the weird
          (and hard) way</h1>
          <h2><small>PGDay Chicago<br>
          2023-04-20</small></h2>
          <div class="source-image">
            Image by <a href=
            "https://pixabay.com/users/pexels-2286921/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1866518">
            Pexels</a> from <a href=
            "https://pixabay.com//?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1866518">
            Pixabay</a>
          </div><img alt="logo EDB" class="logo-image" src=
          "images/common/edb.png">
        </section>
        <section data-background-image=
        "images/sql-new/question.jpg" data-background-opacity="0.3"
        data-background-position="center center">
          <h2>Who am I</h2>
          <ul>
            <li class="fragment fade-in-then-semi-out">Lætitia
            Avrot</li>
            <li class="fragment fade-in-then-semi-out">PostgreSQL
            recognized contributor</li>
            <li class="fragment fade-in-then-semi-out">PostgreSQL
            Europe board member</li>
            <li class="fragment fade-in-then-semi-out"><span class=
            "accent">#PostgresWomen</span> co-founder</li>
            <li class="fragment fade-in-then-semi-out"><span class=
            "accent">EDB</span> Field CTO</li>
            <li class="fragment fade-in-then-semi-out">
              <a href=
              "https://fosstodon.org/web/@l_avrot">@l_avrot@fosstodon.org</a>
            </li>
            <li class="fragment">
              <a href=
              "https://mydbanotebook.org">mydbanotebook.org</a> /
              <a href="https://psql-tips.org">psql-tips.org</a>
            </li>
          </ul>
          <div class="source-image">
            Image by <a href=
            "https://pixabay.com/fr/users/Anemone123-2637160/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=2736480">
            Anemone123</a> from <a href=
            "https://pixabay.com/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=2736480">
            Pixabay</a>
          </div><img class="logo-image" src=
          "images/common/edb.png">
          <aside class="notes">
            PostgreSQL DBA since 2007<br>
          </aside>
        </section>
        <section data-background-image="images/aoc/girl.jpg"
        data-background-opacity="0.2" data-background-position=
        "center center">
          <h2 class="normalcase">Learning advanced SQL the weird
          (and hard) way</h2>
          <ul>
            <li class="fragment fade-in-then-semi-out">Some
            rules</li>
            <li class="fragment fade-in-then-semi-out">Some
            technics</li>
            <li class="fragment fade-in-then-semi-out">What I
            learned</li>
          </ul>
          <div class="source-image">
            Image by <a href=
            "https://pixabay.com/users/libellule789-5876729/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=2771936">
            Наталия Когут</a> from <a href=
            "https://pixabay.com//?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=2771936">
            Pixabay</a>
          </div><img alt="logo EDB" class="logo-image" src=
          "images/common/edb.png">
          <aside class="notes"></aside>
        </section>
      </section>
      <section>
        <section data-background-image="images/aoc/advent.jpg"
        data-background-opacity="0.4" data-background-position=
        "center center">
          <h2>Advent of code</h2>
          <ul>
            <li>
              <a href=
              "https://adventofcode.com/">https://adventofcode.com/</a>
            </li>
            <li>Yearly code challenge</li>
            <li>2 challenges revealed each day</li>
            <li>Become harder and harder (and harder)</li>
            <li>Can be solved even years after</li>
          </ul>
          <div class="source-image">
            Image by <a href=
            "https://pixabay.com/users/thorstenf-7677369/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=4679538">
            Thorsten Frenzel</a> from <a href=
            "https://pixabay.com//?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=4679538">
            Pixabay</a>
          </div><img alt="logo EDB" class="logo-image" src=
          "images/common/edb.png">
          <aside class="notes"></aside>
        </section>
        <section data-background-image="images/aoc/alcohol.jpg"
        data-background-opacity="0.2" data-background-position=
        "center center">
          <h2>Chose your <span class="strike">poison</span>
          language</h2>
          <ul>
            <li>No mandatory language</li>
            <li>As long as it's a Turing complete language</li>
          </ul>
          <div class="source-image">
            Image by <a href=
            "https://pixabay.com/users/pexels-2286921/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1845295">
            Pexels</a> from <a href=
            "https://pixabay.com//?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1845295">
            Pixabay</a>
          </div><img alt="logo EDB" class="logo-image" src=
          "images/common/edb.png">
          <aside class="notes"></aside>
        </section>
        <section data-background-image="images/aoc/thinking.jpg"
        data-background-opacity="0.3" data-background-position=
        "center center">
          <h2>SQL!!!</h2>
          <div class="source-image">
            <a href=
            "https://www.freepik.com/free-photo/attractive-lady-keeping-hand-chin-while-looking-up-blouse-looking-pensive-front-view_16699024.htm#query=thinking%20chin&amp;position=0&amp;from_view=search&amp;track=robertav1_2_sidr">
            Image by 8photo</a> on Freepik
          </div><img alt="logo EDB" class="logo-image" src=
          "images/common/edb.png">
          <aside class="notes"></aside>
        </section>
        <section data-background-image="images/aoc/sign.jpg"
        data-background-opacity="0.3" data-background-position=
        "center center">
          <h2>Some personal rules</h2>
          <ul>
            <li>SQL only (Postgres flavoured)</li>
            <ul>
              <li>SQL functions and procedures allowed</li>
            </ul>
            <li>No extension</li>
            <li>Latest version of Postgres</li>
            <ul>
              <li>So Postgres 16 in Dec. 2022</li>
            </ul>
          </ul>
          <div class="source-image">
            Image by <a href=
            "https://pixabay.com/users/publicdomainpictures-14/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=14089">
            PublicDomainPictures</a> from <a href=
            "https://pixabay.com//?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=14089">
            Pixabay</a>
          </div><img alt="logo EDB" class="logo-image" src=
          "images/common/edb.png">
          <aside class="notes"></aside>
        </section>
      </section>
      <section>
        <section data-background-image="images/aoc/learning.jpg"
        data-background-opacity="0.3" data-background-position=
        "center center">
          <h2>First things I learned</h2>
          <ul>
            <li>I know nothing</li>
            <li>I need the documentation constantly</li>
            <li>I overcomplicate things</li>
            <li>SQL is not meant to do that</li>
          </ul>
          <div class="source-image">
            Image by <a href=
            "https://pixabay.com/users/klimkin-1298145/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1093758">
            svklimkin</a> from <a href=
            "https://pixabay.com//?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1093758">
            Pixabay</a>
          </div>
          <aside class="notes"></aside>
        </section>
        <section data-background-image="images/aoc/pb.jpg"
        data-background-opacity="0.3" data-background-position=
        "center center">
          <h2>First problems I had to solve</h2>
          <ul>
            <li>Making the script idempotent</li>
            <li>Parsing the input</li>
            <li>Removing some intermediate output</li>
          </ul>
          <div class="source-image">
            Image by <a href=
            "https://pixabay.com/users/ryanmcguire-123690/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=438404">
            Ryan McGuire</a> from <a href=
            "https://pixabay.com//?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=438404">
            Pixabay</a>
          </div>
          <aside class="notes"></aside>
        </section>
        <section data-background-image=
        "images/aoc/stormtrooper.jpg" data-background-opacity="0.3"
        data-background-position="center center">
          <h2>Making the script idempotent</h2>
          <ul>
            <li>Vacuum</li>
            <p>→ Can't rollback a single transaction</p>
            <li>Creating and droping objects is difficult to
            maintain</li>
            <p>→ Create everything in a schema!</p>
          </ul>
          <div class="source-image">
            Image by <a href=
            "https://pixabay.com/users/aitoff-388338/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1343772">
            Andrew Martin</a> from <a href=
            "https://pixabay.com//?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1343772">
            Pixabay</a>
          </div>
          <aside class="notes"></aside>
        </section>
        <section data-background-image="images/aoc/tea.jpg"
        data-background-opacity="0.3" data-background-position=
        "center center">
          <h2>Parsing the input</h2>
          <ul>
            <li>Try to keep the raw input</li>
            <li>Add an id</li>
          </ul>
          <pre><code>id integer generated always as identity</code></pre>
          <ul>
            <li>Use copy</li>
          </ul>
          <pre><code>\copy input(value) from 'input.csv' with (null '');</code></pre>
          <div class="source-image">
            Image by <a href=
            "https://pixabay.com/users/distelapparath-2726923/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=4695935">
            Markus Distelrath</a> from <a href=
            "https://pixabay.com//?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=4695935">
            Pixabay</a>
          </div>
          <aside class="notes"></aside>
        </section>
        <section data-background-image="images/aoc/tea.jpg"
        data-background-opacity="0.3" data-background-position=
        "center center">
          <h2>Parsing the input</h2>
          <ul>
            <li>Vacuum analyze</li>
            <li>Index if needed</li>
          </ul>
          <pre><code>create index on production (minute, bluePrintId);
create index on map ((p[1]),(p[0]));
create index on map using gist (p) where value='#';</code></pre>
          <div class="source-image">
            Image by <a href=
            "https://pixabay.com/users/distelapparath-2726923/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=4695935">
            Markus Distelrath</a> from <a href=
            "https://pixabay.com//?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=4695935">
            Pixabay</a>
          </div>
          <aside class="notes"></aside>
        </section>
      </section>
      <section>
        <section data-background-image="images/aoc/bulb.jpg"
        data-background-opacity="0.4" data-background-position=
        "center center">
          <h2>Most useful tools</h2>
          <div class="source-image">
            Image by <a href=
            "https://pixabay.com/users/colin00b-346653/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=3104355">
            Colin Behrens</a> from <a href=
            "https://pixabay.com//?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=3104355">
            Pixabay</a>
          </div>
          <aside class="notes"></aside>
        </section>
        <section data-background-image="images/aoc/mask.jpg"
        data-background-opacity="0.4" data-background-position=
        "center center">
          <h2><code>generate_series</code></h2>
          <ul>
            <li>Postgres function</li>
            <li>Generates numbers (or timestamps)</li>
            <li>between 2 values</li>
            <li>with a given step</li>
          </ul>
          <pre class="fragment"><code>select generate_series(1,max(id))from input) as generated(id)</code></pre>
          <pre class="fragment"><code>select x.*, y.*
from generate_series(minx, maxx) as x(n),
  generate_series(miny, maxy) as y(n)</code></pre>
          <div class="source-image">
            Image by <a href=
            "https://pixabay.com/users/skitterphoto-324082/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=5819709">
            Rudy and Peter Skitterians</a> from <a href=
            "https://pixabay.com//?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=5819709">
            Pixabay</a>
          </div>
          <aside class="notes"></aside>
        </section><section data-background-image="images/aoc/mask.jpg"
        data-background-opacity="0.4" data-background-position=
        "center center">
          <h2><code>generated columns</code></h2>
          <ul>
            <li>columns generated out of other column values</li>
          </ul>
          <pre><code data-line-numbers="2|5-9">create table input (
  id integer generated always as identity,
  opponent text,
  me text,
  shapePointsPartOne integer generated always as (
    case when me='X' then 1
      when me='Y' then 2
      else 3
    end) stored
)</code></pre>
          <div class="source-image">
            Image by <a href=
            "https://pixabay.com/users/skitterphoto-324082/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=5819709">
            Rudy and Peter Skitterians</a> from <a href=
            "https://pixabay.com//?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=5819709">
            Pixabay</a>
          </div>
          <aside class="notes"></aside>
        </section>
        <section data-background-image="images/aoc/mask.jpg"
        data-background-opacity="0.4" data-background-position=
        "center center">
          <h2>The <code>filter</code> clause</h2>
          <pre><code>select sum(value) as first_star
from (
    select value,
           count(*) filter (where value is null) over (order by id) as grp
    from input
)
group by grp
order by first_star desc
fetch first row only
;</code></pre>
          <p>1 subquery and 8 lines of code instead of 4 CTEs and
          63 lines of code</p>
          <div class="source-image">
            Image by <a href=
            "https://pixabay.com/users/skitterphoto-324082/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=5819709">
            Rudy and Peter Skitterians</a> from <a href=
            "https://pixabay.com//?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=5819709">
            Pixabay</a>
          </div>
          <aside class="notes"></aside>
        </section>
        <section data-background-image="images/aoc/dolls.jpg"
        data-background-opacity="0.3" data-background-position=
        "center center">
          <h2>CTEs</h2>
          <ul>
            <li>Better readability</li>
            <li>Helps iterate naturally</li>
            <li>Chain them!</li>
          </ul>
          <div class="source-image">
            Image by <a href=
            "https://pixabay.com/users/anestiev-2736923/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=2737108">
            Christo Anestev</a> from <a href=
            "https://pixabay.com//?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=2737108">
            Pixabay</a>
          </div>
          <aside class="notes"></aside>
        </section>
        <section data-background-image="images/aoc/dolls.jpg"
        data-background-opacity="0.3" data-background-position=
        "center center">
          <h2>CTEs</h2>
          <pre><code>/* get the lastId of the range + the range number (also known as eflNumber) */
with lastId(id, elfNumber) as (...),
/* get the firstId of the range + the range number (also known as eflNumber) */
firstId(id, elfNumber) as (...),
/* get the elfNumber for each value */
elfNumbers(id, elfNumber) as (...),
/* get the total calories for each elf */
totalCalories(value) as (...)

select max(value) as PartOne
from totalCalories;</code></pre>
          <ul>
            <li>Choose your names wisely</li>
            <li>Name the columns</li>
            <li>Don't forget the <code>as</code> keyword</li>
          </ul>
          <div class="source-image">
            Image by <a href=
            "https://pixabay.com/users/anestiev-2736923/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=2737108">
            Christo Anestev</a> from <a href=
            "https://pixabay.com//?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=2737108">
            Pixabay</a>
          </div>
          <aside class="notes"></aside>
        </section>
        <section data-background-image="images/aoc/dolls.jpg"
        data-background-opacity="0.3" data-background-position=
        "center center">
          <h2>Recursive CTEs</h2>
          <pre><code>  with recursive paths(n,p, stop) as (
    ( select 1, array[B] , false
      from input
      where B = any(input.nextvalves))
    union all
    ( select n+1, valve || p, valve = A
      from paths
        inner join input next
          on paths.p[1] = any(next.nextValves)
      /* Doing more than 30 jumps is useless as we have only 30 minutes */
      where n &lt; 30
        and not stop
        /* Preventing cycling */
        and valve &lt;&gt; all(p))
  )</code></pre>
          <div class="source-image">
            Image by <a href=
            "https://pixabay.com/users/anestiev-2736923/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=2737108">
            Christo Anestev</a> from <a href=
            "https://pixabay.com//?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=2737108">
            Pixabay</a>
          </div>
          <aside class="notes"></aside>
        </section>
        <section data-background-image="images/aoc/dolls.jpg"
        data-background-opacity="0.3" data-background-position=
        "center center">
          <h2>Another way to recurse</h2>
          <ol>
            <li>Create a table</li>
            <li>Create a stored procedure to populate the
            table</li>
            <li>Create a query to generate calls to the stored
            procedure</li>
            <li>Use <code>\gexec</code>
              <div class="source-image">
                Image by <a href=
                "https://pixabay.com/users/anestiev-2736923/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=2737108">
                Christo Anestev</a> from <a href=
                "https://pixabay.com//?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=2737108">
                Pixabay</a>
              </div>
              <aside class="notes"></aside>
            </li>
          </ol>
        </section>
        <section data-background-image="images/aoc/print.jpg"
        data-background-opacity="0.3" data-background-position=
        "center center">
          <h2>Removing intermediate output</h2>
          <ul>
            <li>Use <code>\set QUIET on</code></li>
          </ul>
          <pre><code>\set QUIET on
/* Let's move the head */
select $$call moveRope($$ || id || $$);$$
from input,
  generate_series(1,howfar)
  order by id
  \gexec</code></pre>
          <div class="source-image">
            Image by <a href=
            "https://pixabay.com/users/juanjo6560-677093/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=585262">
            juanjo fernandez</a> from <a href=
            "https://pixabay.com//?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=585262">
            Pixabay</a>
          </div>
          <aside class="notes"></aside>
        </section>
        <section data-background-image="images/aoc/comments.jpg"
        data-background-opacity="0.3" data-background-position=
        "center center">
          <h2>Comments</h2>
          <ul>
            <li>Don't be shy</li>
            <li>Helps clarifying your thoughts</li>
            <li>Helps reading your code</li>
            <li>Don't remove your former code</li>
          </ul>
          <div class="source-image">
            Image by <a href=
            "https://pixabay.com/users/pixelcreatures-127599/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=265128">
            Werner Moser</a> from <a href=
            "https://pixabay.com//?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=265128">
            Pixabay</a>
          </div>
          <aside class="notes"></aside>
        </section>
        <section data-background-image="images/aoc/comments.jpg"
        data-background-opacity="0.3" data-background-position=
        "center center">
          <h2>Comments</h2>
          <pre><code>/* Let's create a map.
 * The cave is 7 units wide. The floor is the 7 points with height 0).
 * We'll also add 5 free lines.
 * Thanks to my dad pointing that out, as the rocks first moves
 * right/left and then down, we have to code the last movement
 * differently. This is not very smart. My dad suggested that we
 * instead make the rock appear on the 5th line after the highest
 * rock which should make all moves behave the same way (and is
 * possible because the rock will always be able to go down on the
 * first move.
 */</code></pre>
          <div class="source-image">
            Image by <a href=
            "https://pixabay.com/users/pixelcreatures-127599/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=265128">
            Werner Moser</a> from <a href=
            "https://pixabay.com//?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=265128">
            Pixabay</a>
          </div>
          <aside class="notes"></aside>
        </section>
      </section>
      <section>
        <section data-background-image="images/aoc/advent.jpg"
        data-background-opacity="0.5" data-background-position=
        "center center">
          <h2>Questions?</h2>
          <div class="source-image">
            Image by <a href=
            "https://pixabay.com/users/jonkline-848975/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=3571747">
            Jon Kline</a> from <a href=
            "https://pixabay.com//?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=3571747">
            Pixabay</a>
          </div>
          <aside class="notes"></aside>
        </section>
      </section>
    </div>
  </div>
  <script src="dist/reveal.js"></script> 
  <script src="reveal.js/plugin/notes/notes.js"></script> 
  <script src="reveal.js/plugin/highlight/highlight.js"></script> 
  <script>
      // More info about initialization & config:
      // - https://revealjs.com/initialization/
      // - https://revealjs.com/config/
      Reveal.initialize({
              controls: true,
              progress: true,
              history: true,
              center: true,
              slideNumber: 'c',
              transition: 'slide', // none/fade/slide/convex/concave/zoom
                                                                        // Learn about plugins: https://revealjs.com/plugins/
              plugins: [RevealNotes, RevealHighlight ]
                                                                  });
  </script>
<script src="//localhost:35729/livereload.js?snipver=1" async="" defer=""></script></body>
</html>

